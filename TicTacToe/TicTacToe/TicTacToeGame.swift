//
//  TicTacToeGame.swift
//  TicTacToe
//
//  Created by Student on 2/17/21.
//  Copyright © 2021 Student. All rights reserved.
//

import Foundation

class TicTacToeGame {
    
    var Players = [Player(), Player()]
    var PlayerTurn = 0
    var MoveCount = 0
    var GameInProgress = false
    var GameIsWon = false
    var WinnerName = ""
    var GameIsTie = false
    var GameIsOver: Bool {
        return GameIsWon || GameIsTie
    }
    
    init() {
        Players[0].Name = "Player 1"
        Players[1].Name = "Player 2"
    }
    
    func MoveIsValid(square: Int) -> Bool {
        if square >= 1 && square <= 9 {
            return !(Players[0].HasSquare(square) || Players[1].HasSquare(square))
        }
        else {
            return false
        }
    }
    
    func MakeMove(square: Int) {
        Players[PlayerTurn].MakeMove(square)
        if Players[PlayerTurn].HasWin {
            GameIsWon = true
            WinnerName = Players[PlayerTurn].Name
            GameInProgress = false
        }
        else {
            MoveCount += 1
            if MoveCount == 9 {
                GameIsTie = true
                GameInProgress = false
            }
            else {
                PlayerTurn = (PlayerTurn + 1) % 2
            }
        }
    }
    
    func NewGame() {
        Players[0].ResetMoves()
        Players[1].ResetMoves()
        PlayerTurn = 0
        MoveCount = 0
        GameInProgress = true
        GameIsWon = false
        WinnerName = ""
        GameIsTie = false
    }
}