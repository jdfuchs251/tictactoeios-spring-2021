//
//  Player.swift
//  TicTacToe
//
//  Created by Student on 2/17/21.
//  Copyright © 2021 Student. All rights reserved.
//

import Foundation
import UIKit

class Player {
    
    var Name = ""
    var Moves = [false, false, false, false, false, false, false, false, false]
    var HasWin: Bool {
        return Moves[0] && Moves[1] && Moves[2] || // Horizontals
            Moves[3] && Moves[4] && Moves[5] ||
            Moves[6] && Moves[7] && Moves[8] ||
            Moves[0] && Moves[3] && Moves[6] || // Verticals
            Moves[1] && Moves[4] && Moves[7] ||
            Moves[2] && Moves[5] && Moves[8] ||
            Moves[0] && Moves[4] && Moves[8] || // Diagonals
            Moves[2] && Moves[4] && Moves[6]
    }
    
    var Color: UIColor = UIColor.blackColor()
    var ColorIndex = 0
    
    func MakeMove(square: Int) {
        if square >= 1 && square <= 9 {
            Moves[square - 1] = true
        }
    }
    
    func HasSquare(square: Int) -> Bool {
        return Moves[square - 1]
    }
    
    func ResetMoves() {
        Moves = [false, false, false, false, false, false, false, false, false]
    }
}