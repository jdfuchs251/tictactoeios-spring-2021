//
//  TicTacToeView.swift
//  TicTacToe
//
//  Created by Student on 2/17/21.
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit

@IBDesignable
class TicTacToeView: UIView {
    
    // Arrays containing the moves for each player
    var Player1_Squares = [false, false, false, false, false, false, false, false, false] {
        didSet {
            setNeedsDisplay()
        }
    }
    var Player2_Squares = [false, false, false, false, false, false, false, false, false] {
        didSet {
            setNeedsDisplay()
        }
    }

    // Display colors for each player
    var Player1_Color: UIColor = UIColor.blackColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    var Player2_Color: UIColor = UIColor.blackColor() {
        didSet {
            setNeedsDisplay()
        }
    }
    
    // Overall board dimensions
    var leftBound: CGFloat {
        return CGFloat(20.0)
    }
    var rightBound: CGFloat {
        return CGFloat(bounds.width - 20)
    }
    var topBound: CGFloat {
        return CGFloat(100.0)
    }
    var bottomBound: CGFloat {
        return CGFloat(bounds.height - 100)
    }
    
    // Dimensions for individual board squares
    var unitWidth: CGFloat {
        return CGFloat((rightBound - leftBound) / 3)
    }
    var unitHeight: CGFloat {
        return CGFloat((bottomBound - topBound) / 3)
    }
    
    var padding: CGFloat { // How far in from the unit border an X or O should be drawn
        return CGFloat(20.0)
    }
    
    var aryBoardSquares: [CGRect] { // Array containing the positions of each board square
        var squares: [CGRect] = Array<CGRect>()
        
        // First row
        squares.append(CGRect(x: leftBound + padding, y: topBound + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        squares.append(CGRect(x: (leftBound + unitWidth) + padding, y: topBound + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        squares.append(CGRect(x: (leftBound + 2 * unitWidth) + padding, y: topBound + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        
        // Second row
        squares.append(CGRect(x: leftBound + padding, y: (topBound + unitHeight) + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        squares.append(CGRect(x: (leftBound + unitWidth) + padding, y: (topBound + unitHeight) + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        squares.append(CGRect(x: (leftBound + 2 * unitWidth) + padding, y: (topBound + unitHeight) + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        
        // Third row
        squares.append(CGRect(x: leftBound + padding, y: (topBound + 2 * unitHeight) + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        squares.append(CGRect(x: (leftBound + unitWidth) + padding, y: (topBound + 2 * unitHeight) + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        squares.append(CGRect(x: (leftBound + 2 * unitWidth) + padding, y: (topBound + 2 * unitHeight) + padding, width: unitWidth - 2 * padding, height: unitHeight - 2 * padding))
        
        return squares
    }
    
    // Used by view controller to determine which square a tap gesture touched
    func GetSquare(point: CGPoint) -> Int {
        for i in Range(0..<9) {
            if aryBoardSquares[i].contains(point) {
                return i + 1 // Return the 1-9 square value, not the 0-based index value
            }
        }
        return 0
    }
    
    override func drawRect(rect: CGRect) {
        // First vertical
        var path = UIBezierPath()
        path.moveToPoint(CGPoint(x: leftBound + unitWidth, y: topBound))
        path.addLineToPoint(CGPoint(x: leftBound + unitWidth, y: bottomBound))
        path.lineWidth = 3
        UIColor.blackColor()
        path.stroke()
        
        // Second vertical
        path = UIBezierPath()
        path.moveToPoint(CGPoint(x: rightBound - unitWidth, y: topBound))
        path.addLineToPoint(CGPoint(x: rightBound - unitWidth, y: bottomBound))
        path.lineWidth = 3
        path.stroke()
        
        // First horizontal
        path = UIBezierPath()
        path.moveToPoint(CGPoint(x: leftBound, y: topBound + unitHeight))
        path.addLineToPoint(CGPoint(x: rightBound, y: topBound + unitHeight))
        path.lineWidth = 3
        path.stroke()
        
        // Second horizontal
        path = UIBezierPath()
        path.moveToPoint(CGPoint(x: leftBound, y: bottomBound - unitHeight))
        path.addLineToPoint(CGPoint(x: rightBound, y: bottomBound - unitHeight))
        path.lineWidth = 3
        path.stroke()
        
        DrawPlayerSquares()
    }
    
    func DrawPlayerSquares() {
        for i in Range(0..<9) {
            if Player1_Squares[i] {
                DrawX(aryBoardSquares[i])
            }
            if Player2_Squares[i] {
                DrawO(aryBoardSquares[i])
            }
        }
    }
    
    func DrawX(region: CGRect) {
        let radius = min(region.width, region.height) / 2 // Always draw the X in a true square region
        
        // Top left to bottom right
        var path = UIBezierPath()
        path.moveToPoint(CGPoint(x: region.midX - radius, y: region.midY - radius))
        path.addLineToPoint(CGPoint(x: region.midX + radius, y: region.midY + radius))
        Player1_Color.setStroke()
        path.lineWidth = 5
        path.stroke()
        
        // Bottom left to top right
        path = UIBezierPath()
        path.moveToPoint(CGPoint(x: region.midX - radius, y: region.midY + radius))
        path.addLineToPoint(CGPoint(x: region.midX + radius, y: region.midY - radius))
        path.lineWidth = 5
        path.stroke()
        
    }
    
    func DrawO(region: CGRect) {
        let path = UIBezierPath(arcCenter: CGPoint(x: region.midX, y: region.midY), radius: min(region.width / 2, region.height / 2), startAngle: 0, endAngle: CGFloat(2 * M_PI), clockwise: false)
        Player2_Color.setStroke()
        path.lineWidth = 5
        path.stroke()
    }
    
    func ResetBoard() { // Reset player arrays to trigger board to clear
        Player1_Squares = [false, false, false, false, false, false, false, false, false]
        Player2_Squares = [false, false, false, false, false, false, false, false, false]
    }
}
