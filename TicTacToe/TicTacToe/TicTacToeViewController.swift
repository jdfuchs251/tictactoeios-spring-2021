//
//  TicTacToeViewController.swift
//  TicTacToe
//
//  Created by Student on 2/17/21.
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit

class TicTacToeViewController: UIViewController {
    
    @IBOutlet weak var lblMessage: UILabel!
    var ticTacToeView: TicTacToeView?
    var game = TicTacToeGame()
    
    @IBAction func PressNewGame() {
        game.NewGame()
        ticTacToeView!.ResetBoard()
        lblMessage.text = game.Players[game.PlayerTurn].Name + "'s turn"
    }
    
    @IBAction func TapScreen(sender: UITapGestureRecognizer) {
        if sender.state == .Ended && game.GameInProgress {
            let point = sender.locationInView(ticTacToeView) // Get the point that was touched
            let square = ticTacToeView!.GetSquare(point) // Use view to determine which square (1-9) was touched
            if game.MoveIsValid(square) {
                game.MakeMove(square)
                ticTacToeView!.Player1_Squares = game.Players[0].Moves // Map player moves onto view to trigger redraw
                ticTacToeView!.Player2_Squares = game.Players[1].Moves
                if game.GameIsOver {
                    if game.GameIsWon {
                        lblMessage.text = game.WinnerName + " is the winner!"
                    }
                    else if game.GameIsTie {
                        lblMessage.text = "Game is a tie!"
                    }
                }
                else { // Game is not over - next player's turn
                    lblMessage.text = game.Players[game.PlayerTurn].Name + "'s turn"
                }
            }
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let settings = segue.destinationViewController as? SettingsViewController {
            settings.ticTacToeController = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        ticTacToeView = view as? TicTacToeView // Use to access fields and methods specific to TicTacToeView
    }
    
    override func didRotateFromInterfaceOrientation(fromInterfaceOrientation: UIInterfaceOrientation) {
        //super.didRotateFromInterfaceOrientation(fromInterfaceOrientation) - Deprecated - don't need to call
        ticTacToeView!.setNeedsDisplay() // Reset view display after rotation
    }
}
