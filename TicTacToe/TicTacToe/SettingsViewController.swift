//
//  SettingsViewController.swift
//  TicTacToe
//
//  Created by Student on 2/17/21.
//  Copyright © 2021 Student. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var txtPlayer1Name: UITextField!
    @IBOutlet weak var txtPlayer2Name: UITextField!
    @IBOutlet weak var pckPlayer1Color: UIPickerView!
    @IBOutlet weak var pckPlayer2Color: UIPickerView!
    var ticTacToeController: TicTacToeViewController?
    var colors = ["Black", "Red", "Orange", "Green", "Blue", "Purple"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ColorPickerSetup(pckPlayer1Color) // Bind color data to the pickers
        ColorPickerSetup(pckPlayer2Color)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Get names and set on screen
        txtPlayer1Name.text = ticTacToeController!.game.Players[0].Name
        txtPlayer2Name.text = ticTacToeController!.game.Players[1].Name
        
        // Get colors and set on screen
        pckPlayer1Color.selectRow(ticTacToeController!.game.Players[0].ColorIndex, inComponent: 0, animated: false)
        pckPlayer2Color.selectRow(ticTacToeController!.game.Players[1].ColorIndex, inComponent: 0, animated: false)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Set names in view controller game class
        ticTacToeController!.game.Players[0].Name = txtPlayer1Name.text!
        ticTacToeController!.game.Players[1].Name = txtPlayer2Name.text!
        
        if ticTacToeController!.game.GameInProgress { // Update turn label with new name
            ticTacToeController!.lblMessage.text = ticTacToeController!.game.Players[ticTacToeController!.game.PlayerTurn].Name + "'s turn"
        }
        
        // Set player 1 colors on game screen
        ticTacToeController!.game.Players[0].ColorIndex = pckPlayer1Color.selectedRowInComponent(0)
        ticTacToeController!.game.Players[0].Color = GetColor(pckPlayer1Color)
        ticTacToeController!.ticTacToeView!.Player1_Color = ticTacToeController!.game.Players[0].Color
        
        // Set player 2 colors on game screen
        ticTacToeController!.game.Players[1].ColorIndex = pckPlayer2Color.selectedRowInComponent(0)
        ticTacToeController!.game.Players[1].Color = GetColor(pckPlayer2Color)
        ticTacToeController!.ticTacToeView!.Player2_Color = ticTacToeController!.game.Players[1].Color
    }
    
    func GetColor(picker: UIPickerView) -> UIColor { // Convert picker value to a color
        let row = picker.selectedRowInComponent(0)
        switch colors[row] {
        case "Black": return UIColor.blackColor()
        case "Red": return UIColor.redColor()
        case "Orange": return UIColor.orangeColor()
        case "Green": return UIColor.greenColor()
        case "Blue": return UIColor.blueColor()
        case "Purple": return UIColor.purpleColor()
        default:
            return UIColor.blackColor()
        }
    }
    
    func ColorPickerSetup(picker: UIPickerView) {
        picker.delegate = self // Can set to self because we implemented protocols
        picker.dataSource = self
    }
    
    // Implement UIPickerViewDataSource protocol
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1 // Number of "columns" in the picker
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colors.count // Number of values in the picker
    }

    // Implement UIPickerViewDelegate protocol
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return colors[row] // Bind color array to picker
    }
}
